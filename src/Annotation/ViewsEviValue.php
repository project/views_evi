<?php

namespace Drupal\views_evi\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a views evi value plugin.
 *
 * @Annotation
 */
class ViewsEviValue extends Plugin {

}
